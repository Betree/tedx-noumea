# TEDx Nouméa - Pour prendre de meilleures décisions, reprenons en main l'information !


## Génération (je) ~ intuition

* Le 23 avril dernier, lors du premier tour des élections présidentielles Françaises, près d’un
jeune sur 3 n’est pas allé voter [source](http://www.ifop.com/media/poll/3749-1-study_file.pdf).
C’est pas cas isolé, aux élections législatives qui ont suivi c’est plus d’un jeune sur deux qui n’a
pas participé. Il y a beaucoup de facteurs qui jouent pour expliquer ça, je vais me concentrer sur
un seul d’entre eux.

* Ma génération a grandi avec Internet, Wikipédia, les réseaux sociaux, un téléphone portable qui
fait GPS, caméra, agenda, service de taxi à la demande ou même agence matrimoniale
Elle a aussi grandi avec l’idée qu’on aura probablement dans un futur proche des voitures autonomes,
des robots qui travailleront à notre place, et des drones qui pourront nous livrer des pizzas avant
même qu’on songe à les commander.

* En même temps que tout ça, cette génération a aussi grandi avec un **système politique** qui lui
n’a **pas tellement évolué** ce dernier siècle ⏭.
Système politique **lent et rigide sur son fonctionnement**, sujet à de nombreuses formes de
**conflits d’intérêts et de jeux de pouvoir** primaires qui biaisent les décisions.

Un système qui dépense une énergie et des sommes d’argent incroyables dans **des élections qui
ressemblent souvent plus à une mauvaise télé-réalité** qu’à un vrai débat d’idées
et qui n’encouragent absolument pas les différents candidats à dire la vérité ou à reconnaître leurs
erreurs.

* À une époque où **on vote tous les jours sur internet**, à travers nos clics / j’aime / partages
/ retweet / pétitions / commentaires, je sais pas vous, mais moi ça me paraît dingue que notre
principal levier sur la politique, ce qui régit notre vie en commun, ça soit de **mettre un bout de
papier dans une boite tous les 5 ans**


## Technologie, progrès en sciences sociales + certains exemples de part le monde ~ optimisme

* Envisager d’autres façons de fonctionner

* Le projet democracy.earth vise par exemple à développer les technologies nécessaires à la mise
en place d’une démocratie liquide. ⏭

	- La **Démocratie liquide**, c’est un système dans lequel on peut donner directement son avis pour
	les thématiques qui nous intéressent et déléguer ses voix à amis / intellectuels / assos
	partageant valeurs et en qui on a confiance pour les thématiques pour lesquelles on a pas le temps
	de se renseigner en détails, ou pour lesquels on ne pense pas avoir l'expertise
	nécessaire – avec la possibilité de révoquer

* Mais on peut aussi s’appuyer sur des choses + concrètes et qui ont déjà fait leurs preuves ⏭ :
	- **Suisse**, depuis + d’un siècle, au niveau national démo. semi-directe =
	  référendum facultatif + initiative populaire ⏭

	- On pourrait penser que ce genre d’organisation conduit au chaos, que les gens vont uniquement
	voter **dans le sens de leur intérêt personnel** et qu’on risque vite de se retrouver avec un
	gouvernement qui autorise à payer ses impôts en câlins et en bisous et à licencier son patron
	(c’est peut être des idées pas si stupides qu’elles en ont l’air d’ailleurs… mais c’est un autre
	débat)

	- **Le fait est** que ces dernières années, les Suisses ont refusé deux semaines de congé payés
	supplémentaires et un revenu universel qui leur aurait garantit un salaire confortable à vie sans
	avoir besoin de travailler. Ça paraît dingue non ?

	- Certaines études menées en Europe semblent aller dans ce sens en montrant que **les endroits
	où offre + de moyens pour s’impliquer dans la vie politique présentent en général
	des citoyens mieux informés** et avec un avis + réfléchit

* S’appuyer sur des **initiatives citoyennes** déjà en place: Parlement et citoyens (consultation
	publiques pour permettre à tous de participer à la discussion et à la rédaction de la loi)

* Je suis de ceux qui pensent que si on veut aujourd’hui voir progresser des idées **audacieuses**,
	plus à même de répondre aux grands défis du 21e siècle, il va falloir qu’on songe sérieusement à
	faire évoluer le cadre dans lequel elles évoluent.

* Et si on arrive à trouver le temps ⏭ pour inventer des miroirs à selfie et des fourchettes
	connectées, j'ai aucun doute sur le fait qu'on puisse trouver le temps pour **mettre à jour nos institutions**

* Mais l’un des principaux défis qui se pose à nous tous si on souhaite expérimenter des modèles
 	plus **participatifs**, c’est clairement celui de l’information ⏭


## Défi : l’information ~ réalité

Dans une démocratie (où le peuple est souverain) l’information a un impact majeur sur l’opinion
publique, et a donc une influence sur le pouvoir lui-même.
Notre information, elle a 2 cannaux principaux.

* ⏭ Médias traditionnels (presse écrite – numérique ou papier-, TV, radio…), on reproche un manque
d’indépendance – et ainsi de neutralité

  - On pourrait parler du fait que dans des pays comme la France ou les US, la majorité des grands
  médias est détenue par une toute petite minorité de **multi-millionnaires et de grands
  groupes**. Mais on en apprend en fait beaucoup + en regardant le **contexte économique** dans
  lequel évoluent ces entreprises

  - Le modèle de la publicité, pousse par exemple beaucoup de médias à ne pas être en
	compétition pour obtenir **des abonnés** mais plus pour obtenir du
	**temps d’attention** – ou comme dirait l’ancien PDG de TF1 – **du temps de
	cerveau disponible** ⏭

  - La presse en ligne « gratuite » ne mesure pas la valeur d’un article à combien il aura aidé à
  **nous éclairer, nous informer**, mais à son **nombre de clics et de partages**.

  - Dans ce contexte, pourquoi mettre en avant un groupe citoyen qui tenterait de rénover la
  politique quand parler du dernier scandale dans la vie privée d’un politicien célèbre nous assure
  **beaucoup + d’attention**

  - **Pourquoi parler des choses qui vont bien**, des associations qui font avancer des causes
  depuis des années quand la meilleure façon de **s’assurer des revenus** reste de jouer sur
  **la peur, le sensationnalisme**, ou encore en sortant des dernières rumeurs en exclusivité quitte à ne pas bien vérifier si elles sont fondées ou **quel impact leur révelation pourrait avoir**

  - ⏭ Ex : Articles QI des Français (étude de 4 pages, échantillon de seulement 79 personnes de
  30 à 63 ans qui ne constituent absolument pas un panel représentatif de la population FR.
	l’étude elle-même précise que les données doivent être enrichies et recoupées avant de pouvoir serieusement tirer ce genre de conclusions).

  - Le noyau de l’information, la donnée a toujours un **contexte d’émission** et une **façon d’être
  présentée** auxquels on doit faire attention.

  - Si les médias traditionnels ne sont pas nécessairement neutres au fond, c’est pas très grave.
  Parce qu’on ne leur demande pas d’être neutres, mais d’être **critiques et pointilleux**.
  Si le but des médias est de nous éclairer, faire progresser les idées et les débats dans le sens
  du bien commun, alors il est important qu’ils puissent prendre position.

  - C'est à nous de vérifier que cette position soit **argumentée et sourcée**.
  Un média sain devrait toujours mentionner les **potentiels conflits d’intérêts** auxquels il
  s’expose  lorsqu’il publie un article et la façon dont il mène ses **investigations**
  devrait toujours être la plus **transparente** possible.

* ⏭ Journalisme citoyen (blogs, plateformes vidéo en ligne, post réseaux sociaux…)

  - (même s’il n’en a pas l’exclusivité) Générateur de **fake news (en fr : des bobards)** dans
  lesquels il nous est tous arrivé de tomber ⏭

  - Si les **sites parodiques** nous en montrent le côté drôle et décalé tout en jouant un rôle
  **éducatif**, les fausses informations (et plus généralement, le peu de rigueur journalistique)
  peuvent avoir des conséquences très néfastes sur le débat public.

  - Il est par exemple extrêmement difficile d’avoir un débat constructif en ligne à propos
  de sujets importants comme la vaccination sans voir réapparaître tout une série d’articles basés
  sur des études plusieurs fois démenties. ⏭

  - **Bulles d’information**: Facebook dont l’objectif commercial est de nous garder le + longtemps
  possible sur leurs pages, a tout intérêt à nous montrer du contenu qui va dans le sens de nos
  intérêts **mais aussi de nos opinions**, nous plongeant petit à petit dans une chambre d’échos où
  résonnent surtout les idées qui nous confortent dans nos positions

* D’un côté donc des médias traditionnels qui peinent parfois à se détacher de leur cadre politique
et économique, de l’autre une information abondante mais beaucoup moins vérifiée, et au milieu
nous tous qui consommons de l’information au quotidien

  - **Temps d’attention** toujours + court
  - Nombreux **biais**, notamment nos biais de confirmation (ex : 99 articles factuels attestant des
    changements climatiques à quelqu’un qui n’y croit pas) ⏭
  - **Pourquoi vouloir entendre la vérité, quand on peut entendre qu’on a raison ?**

* On sait donc qu’aucune de nos sources n’est fiable à 100% et qu’on doit se méfier de tout ce qu’on
voit, mais à part les journalistes dont c’est le métier, on a pas tous le temps de croiser nos
sources et de vérifier chaque chiffre pour tout ce qu’on voit et ce qu’on entend sur Internet.


## Solutions : CaptainFact ~ conclusion, actions

Et pourtant…

* ⏭ **Wikipedia**: plateforme collaborative sur laquelle multiplicité + diversité des contributions
créent une forme de neutralité

* Fact: Fiabilité Wiki (notamment parce que le site demande systématiquement des sources)

* Et si on pouvait appliquer les mêmes principes avec l’info du quotidien

* Il y a bientôt un an, CaptainFact, matérialiser cette idée en créant & expérimentant des **outils
numériques** qui pourraient nous aider à nous informer de manière + intelligente ⏭

  - ⏭ Le 1er de ces outils vient superposer infos sur les vidéos pour **confirmer, infirmer, sourcer**

  - ⏭ Une autre fonctionnalité qui elle est encore en développement: En lisant articles, voir points
  sur lesquels faire attention **avec des liens vers sources potentiellement contradictoires**

  - ⏭ Toutes ces infos, fournies par la communauté, sont regroupées sur une plateforme pour débattre
  de manière **beaucoup + précise** que ce que permettent aujourd’hui les différents systèmes de
  commentaire, en travaillant **point par point, argument par argument**, au lieu de traiter l'ensemble d'un bloc: on peut être d'accord avec certaines
	parties d'un contenu, et en désaccord avec d'autres.

* Le tout avec des mécaniques de jeu comme la réputation ou des médailles pour faire remonter +
facilement le contenu de qualité

* Un commentaire sourcé sur CaptainFact rapporte par exemple toujours + de points qu’un simple
commentaire d’opinion

## Ouverture

* Si tout ça peut nous aider à mieux consommer et digérer l’information, un outil résout rarement
tous les problèmes (guerres d’édition wiki pharma, #astroturf)

* Si on veut demain construire des sociétés plus participatives, il y a un travail que rien ni
personne ne pourra faire à notre place : celui de développer et d’endurcir nos esprits critiques

* Il y a dors et déjà des petites choses auxquelles on peut faire attention au quotidien pour aller
dans ce sens

  - Acceptons qu’il n’y a souvent pas, dans nos opinions politiques, de vérité absolue puisque
    celle-ci sont fondées sur nos interprétations personnelles de ce que doivent être la liberté,
    l’égalité, et plus généralement la vie en commun
  - Suivre des médias, et débattre avec des gens aux opinions différentes (pour percer la bulle)
  - Débattons en attaquant toujours les idées - pas les personnes. Critiquons le jeu, pas les joueurs
  - Être sceptique de tout, ne pas croire sans preuve (je vous parle pas de religion)
  - Se méfier du hooligan politique qui sommeille en nous. Vous savez, celui qui nous pousse à
    cliquer sur partager avant d’avoir lu l’article ? (on l’a tous fait)
  - Acceptons peut-être de devoir payer pour assurer l’indépendance des médias qui en valent la
    peine. Ou au moins garder à l’esprit que l’information gratuite n’est jamais réellement
    gratuite : quelqu’un d’autre l’a payée pour nous.
