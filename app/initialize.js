import "impress.js"


impress().init()

let rootElement = document.getElementById("impress");


rootElement.addEventListener( "impress:stepenter", function() {
  let currentStep = document.getElementsByClassName("step has-video active")[0];
  if (!currentStep)
    return true;
  let video = currentStep.getElementsByTagName('video')[0];
  video.currentTime = 0;
  video.play();
});

rootElement.addEventListener( "impress:stepleave", function(event) {
  const currentStep = event.target
  const stepVideos = currentStep.getElementsByTagName('video')
  if (stepVideos.length > 0)
    stepVideos[0].pause()
  //TODO Stop video
});