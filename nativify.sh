#!/usr/bin/env bash

# TODO
#brunch watch -s -p -P 4242 &
#sleep 5 # Wait for the server to start
#
#echo "Starting nativefier"
#sleep 10
#

nativefier -p linux   --full-screen --disable-context-menu --disable-dev-tools -n tedx http://localhost:3333 bin
nativefier -p osx     --full-screen --disable-context-menu --disable-dev-tools -n tedx http://localhost:3333 bin

# Requires wine
# nativefier -p windows --full-screen --disable-context-menu --disable-dev-tools -n tedx http://localhost:3333 bin
